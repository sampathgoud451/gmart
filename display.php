<?php


include 'links.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Display Table</title>
</head>
<body>
    <?php
    
    include 'dataconnect.php';

    $selectquery = " SELECT * from registration  ";
    if(isset($_POST['search'])) {
        $search_data =($_POST['search_box']);

        $selectquery .= " where first_name = '{$search_data}' ";
    }


    $query = mysqli_query($con ,  $selectquery);
    $res = mysqli_fetch_array($query);
    
    ?>
    <div class="container mt-3 table-responsive">
        <form action="display.php"  method="POST" name="search_form">
            <input type="text" name="search_box" value="search"  />
            <input type="submit" name="search" value="Search here..." />
            <a href="index.php">Index</a>
            <table class="table   table-bordered mt-5">
                <thead>
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">FIRSTNAME</th>
                    <th scope="col">LASTNAME</th>
                    <th scope="col">GENDER</th>
                    <th scope="col">CONTACT</th>
                    <th scope="col">USERNAME</th>
                    <th scope="col">PASSWORD</th>
                    <th scope="col">REPASSWORD</th>
                    <th scope="col">OPERATION</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                
                    
                    
                    while($res = mysqli_fetch_array($query)){

                ?>
                        <tr>
                        <th scope="row  "><?php echo $res['id'] ?></th>
                        <td><?php echo $res['first_name'] ?></td>
                        <td><?php echo $res['last_name'] ?></td>
                        <td><?php echo $res['gender'] ?></td>
                        <td><?php echo $res['contact'] ?></td>
                        <td><?php echo $res['user_name'] ?></td>
                        <td><?php echo $res['password'] ?></td>
                        <td><?php echo $res['repassword'] ?></td>
                        <td><a href="update.php?id=<?php echo $res['id'] ?>"  name="update" value="">UPDATE</a></td>
                        <td><a href="delete.php?id=<?php echo $res['id'] ?>" name="delete" value="">DELETE</a></td>
                        </tr>
                <?php
                    }
                ?>
                    
                </tbody>
            </table>
        </form>
    </div>
</body>
</html>

