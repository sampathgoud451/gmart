


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Report card</title>
    <?php include 'links.php' ?>
</head>
<body>

    <div class="main-div container">
        <h1 class="text-center">DATA INFORMATION</h1>
        <div class="table-responsive  ">
            <table class="table  align-middle table-bordered">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>firstName</th>
                        <th>lastname</th>
                        <th>gender</th>
                        <th>contact</th>
                        <th>user_name</th>
                        <th>password</th>
                        <th>re_password</th>
                    </tr>
                </thead>

                <tbody>
                <?php

                    include 'dataconnect.php';

                    $selectquery = "select * from registration";

                    $query = mysqli_query($con , $selectquery);

                    $nums = mysqli_num_rows($query);

                    $res = mysqli_fetch_array($query);

                        for(  $i=0; $i<count($res); $i++){
                ?>   
                            <tr>
                                <td><?php echo $res['id'];  ?></td>
                                <td><?php echo $res['first_name'];  ?></td>
                                <td><?php echo $res['last_name'];  ?></td>
                                <td><?php echo $res['gender'];  ?></td>
                                <td><?php echo $res['contact'];  ?></td>
                                <td><?php echo $res['user_name'];  ?></td>
                                <td><?php echo $res['password'];  ?></td>
                                <td><?php echo $res['repassword'];  ?></td>
                            </tr>

                <?php      
                        }

                ?>

                    
                </tbody>
            </table>
        </div>
        
    </div>
    
</body>
</html>