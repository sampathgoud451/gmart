<!--footer-->
<section >
    <footer>
        <div class="container-fluid   footer_bg py-3">
            <div class="row  py-0 ">
                <div class="col-lg-3 col-md-3 col-sm-3 lead text-center " >
                    <p class="  footer_para ">Copy-Rights &copy; 2022 </p>
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 text-center ">
                    <p class="  text-center footer_para">Phone:9853245185</p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 text-center ">
                    <h6 class=" text-center promotion">Shop More , Pay less..<span>Thank you</span></h6>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 text-center ">
                    <a href="#" class="position-absolute bottom-0 end-0 py-2">
                    <i class="bi bi-arrow-up-circle h1" id="arrow"></i>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</section>