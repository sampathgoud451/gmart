<!--header-->

<nav class="navbar navbar-expand-lg-md-sm navbar-dark bg-dark p-2" id="header-main">
        <div class="container d-flex d-flex-row ">
           <a class="navbar-brand" href="index.html"><img class="header-img img-fluid" src="header-images/LOGO.jpg" alt=""></a>
           <!-- <h1 class="text-light text-center">Shopping <span class="text-muted">Mart</span></h1>-->
          
               <a class="navbar-title  justify-content-center" href="index.html"><img src="header-images/gmart-title.jpg" width="500" height="100" alt=""></a>
        
            <div class="flex-column ">
                
              <!-- <h4 class="text-success me-auto" ><b>Address:</b></h4>-->
                <p class="text-light lead " id="para">Badangpet</p>
                <p class="text-light lead" id="para">Road-NO-4</p>
                <p class="text-light lead" id="para">opp:HP petrol pump</p>
                <div class="d-flex flex-row">
                   <button class="btn btn-sm   btn-light" type="button"  id="login" >Login</button>
                    <span class="me-3"></span>
                     <a  href="register.html" target="_blank" id="regist">Register</a>
                </div>
            </div>
            
                
        </div>
             
    </nav>

    <!---nav menu bar-->

    <section>
        <nav class=" navbar nav-tabs navbar-expand-lg navbar-primary bg-primary p-0 flex-sm-column">
            <div class="container">
                <ul class="nav  ms-auto">
                    <li class="nav-item">
                        <a class="nav-link active link-light" href="index.php">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link  link-light" href="contact.php">contact us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link-light" href="">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link-light" href="">Gallery</a>
                    </li>
                </ul>
            </div>
        </nav>
    </section>

     <!--hidden login form-->
    <div class="showForm d-none d-flex justify-content-end me-4" id="formVisible" > 
       
        <form >
            <div class="mb-2">
                <label for = "Email" class="form-label">Email Address</label>
                <input type="email" class="form-control" >
            </div>
            <div class="mb-2">
                <label for = "password" class="form-label">Password</label>
                <input type="password" class="form-control" >
            </div>
            <div class="d-flex d-flex-row">
                <button type="submit" class="btn btn-primary">Submit</button>
                <span class="mx-3"></span>
                <a class="button-link text-danger " href="#"><p>Forgot Password?</p></a>
            </div>
        </form>
    </div>








     <!--footer-->
     <section class="bg-secondary ">
        <div class="container bg-secondary">
            <diV class="row align-items-center">
                <div class="col-4 d-flex d-flex-row justify-content-start ">
                    <a href="#"><i class="bi bi-twitter text-primary mx-2"></i></a>
					<a href="#"><i class="bi bi-facebook text-primary mx-2"></i></a>
					<a href="#"><i class="bi bi-linkedin text-blue mx-2"></i></a>
					<a href="#"><i class="bi bi-instagram text-danger mx-2"></i></a>
                </div>
                <div class="col-4 d-flex d-flex-row justify-content-center align">
                    <p class="lead text-white  py-2">Copy-Rights &copy; 2022 </p>
                    <a href="#" class="position-absolute bottom-0 end-0 py-2">
                        <i class="bi bi-arrow-up-circle h1" id="arrow"></i>
                    </a>
                </div>
                <div class="col-4 d-flex d-flex-row justify-content-end  ">
                    <p class="text-light  "><b>Phone:</b></p>
                    <span class="me-1"></span>
                    <p class="text-light ">9853245185</p>
                </div>
            </diV>
        </div>
    </section>
    
