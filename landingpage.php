
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php  include 'links.php';  ?>
    

    <style>
        
        .bg_img{
        background-image: url('https://cdn.pixabay.com/photo/2020/05/21/18/52/supermarket-5202138__480.jpg');
        height:100vh;
        width:100%;
        position:relative;
        background-repeat: no-repeat;
        background-size: cover;
        
    }
    </style>
</head>
<body>
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
    <div class="bg_img">
        <section>
            <div class="row">
            
            <div class="container ">
                <div class="row">
                    <div class=" col-lg-12 col-ms-12 col-sm-12    login_card" >
                        <h1 class="text-dark text-center login_title">Login form</h1>
                        <form action="loginvalidation.php" method="POST">
                            <div class="form-group mt-5 mt-sm-2">
                                <label class="text-dark">USERNAME</label>
                                <input type="text" name="user" class="form-control  " placeholder="Enter Your username">
                            </div>
                            <div class="form-group mt-3">
                                <label class="text-dark">PASSWORD</label>
                                <input type="password" name="password" class="form-control " placeholder="Enter Your Password">
                            </div>
                            <div class="login_btn">
                                <button type="submit" name="submit" class="btn btn-primary mt-3">Login</i></button>
                            </div>
                            <div class="reg_info">
                                 <p class="text-dark">haven't account? , let's <a href="register.php" name="register" class="land_reg">Register </a>
                                   here</p>
                            </div>
                        </form>
                    </div>   
                </div>
            </div>
        
            </div>
        </section>
        <h1 class="text-light text-center my-3 welcome">Welcome to G-Mart</h1>
        <div class="gmart_caption ">
            <h1>Shop More , Pay Less...</h1>
        </div>
    </div>
    </div>
    </div>
</body>
</html>







     
    
