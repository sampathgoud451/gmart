<?php


include 'links.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User contact display table</title>
</head>
<body>
    <?php
    
    include 'dataconnect.php';

    $selectquerydata = " SELECT * from  contact_form ";

    $query = mysqli_query($con ,  $selectquerydata);
    $result = mysqli_fetch_array($query);
    
    ?>


    <div class="container mt-3 table-responsive">
        <form action="usercontactdisplay.php"  method="POST" name="search_form">
            <input type="text" name="search_box" value="search"  />
            <input type="submit" name="search" value="Search here..." />
            <a href="index.php">Index</a>
            <table class="table   table-bordered mt-5">
                <thead>
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">NAME</th>
                    <th scope="col">EMAIL-ADRESS</th>
                    <th scope="col">COMMENT</th>
                    <th scope="col">OPERATION</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    while($res = mysqli_fetch_array($query)){
                ?>
                        <tr>
                        <th scope="row  "><?php echo $result['id'] ?></th>
                        <td><?php echo $result['name'] ?></td>
                        <td><?php echo $result['email_adress'] ?></td>
                        <td><?php echo $result['comment'] ?></td>
                        <td><a href="update.php?id=<?php echo $result['id'] ?>"  name="update" value="">UPDATE</a></td>
                        <td><a href="delete.php?id=<?php echo $result['id'] ?>" name="delete" value="">DELETE</a></td>
                        </tr>
                <?php
                    }
                ?>
                    
                </tbody>
            </table>
        </form>
    </div>
</body>
</html>