 <!--including all links through php include--> 
<?php include 'links.php' ?>
<!----style sheet for error meassages-->  
<style>
    .error{
        color:#8f4507;
    }
    #update{
        color:white;
        text-align : center;
        display:none;
    }
    
        
        .bg_reg_img{
        background-image: url('https://cdn.pixabay.com/photo/2020/05/21/18/52/supermarket-5202138__480.jpg');
        height:100vh;
        width:100%;;
        position:relative;
        background-repeat: no-repeat;
        background-size: cover;
    }

    body{
        width: 100vw!important;
        height: 100vh!important;

    }
</style>
    

<body  class="bg_reg_img">
        



    <!--registration form validations--> 
    <?php include 'regvalidation.php'; ?>


    <!--includes-->  
    <?php // include 'commonheader.php' ?>

    <!--contact section content-->

    <div class="container">
        <div class="row" >

             <!--image dispaly-->
             <div class="col-lg-4 col-md-12 col-sm-12 offset-lg-1 offset-md-0 offset-sm-0 mt-lg-5  mt-sm-1">
                <div class="card register_card_image">
                    <img src="https://img.freepik.com/free-vector/online-registration-concept_23-2147980084.jpg" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title">Lets Regiser for more Updates and get access of Everything</h5>
                        <a href="" class="btn btn-primary">ReadMore</a>
                    </div>
                </div>
            </div>

            <!--form-->
            
            <div class="col-lg-4 col-md-12 col-sm-12 offset-lg-1 offset-md-0 offset-sm-0 mt-lg-5 mt-sm-1">
                     <div class="box  bg-dark   "  >

                        <form class="form_box"  action="" method="POST">
                           
                               <!-- <label for = fname class="form-label">First Name</label>-->
                                <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name">
                               <!-- <input type="text" class="form-control" placeholder="First Name">-->
                                <span class="error"><p>* <?php echo  $fnameErr;  ?></p></span>
                            
                            
                               <!-- <label for = lname class="form-label">Last Name</label>-->
                                <input type="text" class="form-control " id="lname" name="lname" placeholder="Last Name">
                                <span class="error"><p>* <?php echo  $lnameErr;  ?></p></span>   
    
                
                                   <!-- <label for = "phone" class="form-label text-light">Contact</label>-->
                                    <input type="text" class="form-control " id="cell" name="phone" placeholder="contact">  
                                    <span class="error"><p>* <?php echo $contactErr;?></p></span> 

                
                                   <!-- <label for = "userName" class="form-label">UserName</label>-->
                                    <input type="email" class="form-control " id="mail" name="email" placeholder="User Name">  
                                    <span class="error"><p>* <?php echo $userErr;?></p></span>  
            
                        
                                   <!-- <label for = "password" class="form-label">Create Password</label>-->
                                    <input type="password" class="form-control " id="name" name="pass" placeholder="Password">   
                                    <span id="passwordHelpInline" class="form-text text-info">
                                    <span class="error"><p>* <?php echo $passErr;?></p></span> 
                                    </span>
            
                    
                                   <!-- <label for = "re-password" class="form-label">ReEnter Password</label>-->
                                    <input type="password" class="form-control" id="name" name="repass" placeholder="Repassword">  
                                    <span class="error"><p>* <?php echo $repassErr;  ?></p></span>  
        
                            <button type="submit" class=" regist_form_button bg-primary" name="submit" value="Register" >Register</button>
                            <a href="display.php" name="display" class="link data_display bg-primary">DATA-DISPLAY</a>
                            <a href="landingpage.php"  name="login" class="link bg-primary login_from_regist_form">Login</a>
                        </form>
                        
                    </div>
                </div>
            </div>


            
           
            <!--images dispaly-->
            <!--<div class="col-lg-4 col-md-6 col-sm-12 mt-5 ">
                    <img src="images/regist2.png" alt="">
                    <h1>Follow us on</h1>
                <ul class="socio">
                    <li > <a href="#"><i class="bi bi-twitter text-primary mx-1"></i></a></li>
                    <li> <a href="#"><i class="bi bi-facebook text-primary mx-1"></i></a></li>
                    <li><a href="#"><i class="bi bi-linkedin text-primary mx-1"></i></a></li>
                     <li><a href="#"><i class="bi bi-instagram text-danger mx-1"></i></a></li>
                </ul>
            </div>-->

        </div>

    </div>


      <!--footer-->
    <!--<section class="bg-dark ">
        <div class="container bg-dark ">
            <diV class="row align-items-center">
                <div class="col-4 d-flex d-flex-row justify-content-start ">
                    <a href="#"><i class="bi bi-twitter text-primary mx-2"></i></a>
					<a href="#"><i class="bi bi-facebook text-primary mx-2"></i></a>
					<a href="#"><i class="bi bi-linkedin text-blue mx-2"></i></a>
					<a href="#"><i class="bi bi-instagram text-danger mx-2"></i></a>
                </div>
                <div class="col-4 d-flex d-flex-row justify-content-center">
                    <p class="lead text-secondary ">Copy-Rights &copy; 2022 </p>
                   <!-- <a href="#" class="position-absolute bottom-0 end-0 py-2">
                        <i class="bi bi-arrow-up-circle h1" id="arrow"></i>
                    </a>-->
               <!-- </div>
                <div class="col-4 d-flex d-flex-row justify-content-end  ">
                    <p class="text-secondary  "><b>Phone:</b></p>
                    <span class="me-1"></span>
                    <p class="text-secondary ">9853245185</p>
                </div>
            </diV>
        </div>
    </section>-->
    

    <script>
       /* $(document).ready(function(){
            $("#showUpdate").click(function(){
             alert('Form Submitted');
         });
        });
    </script>
    <?php
    
    ?>
</body>
</html>


<?php


  /*sql database connnection and insertions*/
include 'dataconnect.php';

    if(isset($_POST['submit'])){
        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $contact = $_POST['phone'];
        $username = $_POST['email'];
        $password = $_POST['pass'];
        $repass = $_POST['repass'];

        $res = 0;
        if($validate == TRUE){
            $insertquery = "insert into registration(first_name , last_name  , contact , user_name , password , repassword) values('$fname' , ' $lname' , '$contact'
            , '$username' , '$password' , '$repass')";
        
            $res = mysqli_query($con , $insertquery);
        
        }

        if($res) {
            ?>
            <script>
                alert("data inserted");
            </script>
            <?php
        } else {
            ?>
            <script>
                alert("data not inserted");
            </script>
            <?php
        }
    }

?>



