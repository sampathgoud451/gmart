<!--header-->
<style>
   body{
       Width: 100%;
       height: 100vh;
       font-family: 'Roboto', sans-serif;
       line-height: 6px;
   }
   p{
       font-family: 'Roboto', sans-serif;
       line-height: 6px;
       font-size: 16px;
       font-weight: 300;
   }
   a{
    font-family: 'Roboto', sans-serif;
    font-size: 20px;
    font-weight: 300;
    color: #fff;
   }
</style>


<?php include 'links.php'  ?>
<header>
    <div class="row align-items-center  ">
        <nav class="navbar navbar-expand-lg-md-sm navbar-light  p-2  main-header_bg" >
            <div class="container">
                    <div class="col-lg-md-sm-4 ">
                    <h1 class="animate_word">
                    <span>we have</span>
                            <div>
                                <ul class="flip">
                                    <li>Groceries</li>
                                    <li>HomeNeed</li>
                                    <li>Pulses</li>
                                    <li>Vegetable</li>
                                    <li>Toys</li>
                                    <li>Clothes</li>
                                    <li>ChickNeeds</li>
                                    
                                </ul>
                            </div>
                    </h1>
                        <a class="navbar-brand" href="index.html"><img class="header-img img-fluid" src=" " alt=""></a>
                        <!--navbar toggler-->
                        
                        <!-- <h1 class="text-light text-center">Shopping <span class="text-muted">Mart</span></h1>-->
                    </div>
                    <div class="col-lg-4 web_title">
                        <h1 class="navbar-title justify-content-center text-light main_title"><span class="big_letter">G</span><span class="text_remove">mart<span></h1>
                    </div>
                    <div class="col-lg-md-sm-4 text-end">
                            
                        <!-- <h4 class="text-success me-auto" ><b>Address:</b></h4>-->
                        <div class="address_card_details">
                            <p >Badangpet</p>
                            <p >Road-NO-4</p>
                            <p >opp:HP petrol pump</p>
                        </div>
                           <a  href="" id="login">Login</a>
                            <a  href="register.php" class="text-light" id="regist">Register</a>
                            <a href="logout.php" class="logout_button"  >Logout</a>
                    </div> 
            </div>  
        </nav>
    </div>
</header>
 
    <!---nav menu bar-->

    <section>
        <nav class=" navbar  navbar-expand-lg navbar-light menu_bar_bg p-0 flex-sm-column">
            <div class="container">
                <div class="row">
                    <div class="col-lg-md-sm-12">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="nav  ms-auto">
                                <li class="nav-item ">
                                    <a class="nav-link nav_menu_item " href="index.php">Home</a>
                                </li>
                                <li class="nav-item  ">
                                    <a class="nav-link nav_menu_item " href="contact.php">Contact-us</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link nav_menu_item  " href="">About</a>
                                </li>
                                <li class="nav-item ">
                                    <a class="nav-link nav_menu_item  " href="">Gallery</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </section>

     <!--hidden login form-->
    <div class="showForm d-none d-flex justify-content-end me-4" id="formVisible" > 
       
        <form >
            <div class="mb-2">
                <label for = "Email" class="form-label">Email Address</label>
                <input type="email" class="form-control"  name = "email">
            </div>
            <div class="mb-2">
                <label for = "password" class="form-label">Password</label>
                <input type="password" class="form-control" name="password" >
            </div>
            <div class="d-flex d-flex-row">
                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                <span class="mx-3"></span>
                <a class="button-link text-danger " href="#"><p>Forgot Password?</p></a>
            </div>
        </form>
    </div>








     
    
