<?php
    /* registration form validation */
    $fnameErr = $lnameErr  = $contactErr = $userErr = $passErr = $repassErr ="";
    $fname = $lname  = $contact = $user = $pass = $repass = $update = "";
    

    $validate = TRUE;

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        /*firstname*/
        if(empty($_POST['fname'])) {
          echo  $fnameErr = "FirstName is required";
          $validate = FALSE;
        } 
        elseif (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['fname']))  {
            echo $fnameErr = "Alpha and spaces only be allowed!";
            $validate = FALSE;
        }
        /*lastname*/
        elseif(empty($_POST['lname'])) {
            echo $lnameErr = "LastName is required";
            $validate = FALSE;
        }
        elseif  (!preg_match("/^[a-zA-Z-' ]*$/", $_POST['lname'])) {
            echo  $lnameErr = "Alpha and spaces only be allowed!";
            $validate = FALSE;
            
         
        /*gender*/
        /*elseif (empty($_POST["gender"])) {
            echo $genderErr = "gender is required!";
            $validate = FALSE;*/
        }
        /*contact*/ 
        elseif (empty($_POST["phone"])) {  
            echo $contactErr = "Mobile no is required"; 
            $validate = FALSE; 
        } 
        elseif (!preg_match ("/^[0-9]*$/", $_POST['phone'])) {  
            echo $contactErr = "only numbers are allowed!";
            $validate = FALSE;
        }  
        elseif($contact > 10){
            echo $contactErr = "number must be 10 charecters only";
            $validate = FALSE;
        }

        /*username*/ 
        elseif(empty($_POST['email'])){
            echo  $userErr = "UserName must be required";
            $validate = FALSE;
        }
        elseif(strlen($_POST['email'] > 25)){
            echo $userErr = "invalid username";
            $validate = FALSE;
        }/* elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo $userErr = "proper username is required";
        } */

        /*password*/
        elseif(empty($_POST['pass'])){
            echo $passErr = "password is required";
            $validate = FALSE;
        } 
        elseif (strlen($_POST["pass"]) < 8) {
            echo  $passErr = "Your Password Must Contain At Least 8 Characters!";
            $validate = FALSE;
        } 
        

        /*repassword*/  
        
        elseif(empty($_POST['repass'])){

            echo  $repassErr = "password must be entered";
            $validate = FALSE;
        }
        elseif($pass != $repass){
            
            echo   $repassErr = "paswords must be matched";
            $validate = FALSE;
        }
        else {
            $fname = test_input($_POST["fname"]);
            $lname = test_input($_POST["lname"]);
            //$gender = test_input($_POST["gender"]);
            $contact = test_input($_POST["phone"]);
            $user = test_input($_POST["email"]);
            $pass = test_input($_POST["pass"]);
            $repass = test_input($_POST["repass"]);
        }

        
    
    
    }



    /*function*/
   function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    ?>
    